package com.bachelor;

public class Main {

    public static void main(String[] args) {
        FileHandler fileHandler = new FileHandler();
        QuickSort quickSort = new QuickSort();
        MergeSort mergeSort = new MergeSort();
        InsertionSort insertionSort = new InsertionSort();
        SelectionSort selectionSort = new SelectionSort();


        System.out.println("## Sorting has started. ##");

        //  -- ONLY CHANGE HERE BELOW  --
        // Change file name and type.
        int[] testdata = fileHandler.readInDataInteger("testInt_3.txt");

        long startTime = System.nanoTime();
        //Change type and sorting algorithm.
        int[] sortedArray = insertionSort.insertionSortInt(testdata);
        long timeItTook = System.nanoTime() - startTime;
        // -- ONLY CHANGE ABOVE THIS COMMENT --

        System.out.println("## Sorting has ended. ##");
        System.out.println("Sorting time is was:");
        System.out.println(timeItTook / 1000000);

    }
}
