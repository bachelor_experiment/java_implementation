package com.bachelor;

import java.io.File;
import java.util.Scanner;

public class FileHandler {
    private String testDataFolder = "../../../../testData/";

    public int[] readInDataInteger(String fileName) {
        int [] intArray = new int[1];
        try {
            File file = new File(this.testDataFolder + fileName);
            Scanner fileReader = new Scanner(file);
            intArray = new int[Integer.parseInt(fileReader.nextLine())];
            int positionInArray = 0;
            while (fileReader.hasNextLine()) {
                intArray[positionInArray] = Integer.parseInt(fileReader.nextLine());
                positionInArray++;
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
        return intArray;
    }

    public char[] readInDataChar(String fileName) {
        char[] charArray = new char[1];
        try {
            File file = new File(this.testDataFolder + fileName);
            Scanner fileReader = new Scanner(file);
            charArray = new char[Integer.parseInt(fileReader.nextLine())];
            int positionInArray = 0;
            while (fileReader.hasNextLine()) {
                String line = fileReader.nextLine();
                charArray[positionInArray] = line.toCharArray()[0];
                positionInArray++;
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
        return charArray;
    }

    public float[] readInDataFloat(String fileName) {
        float[] floatArray = new float[1];
        try {
            File file = new File(this.testDataFolder + fileName);
            Scanner fileReader = new Scanner(file);
            floatArray = new float[Integer.parseInt(fileReader.nextLine())];
            int positionInArray = 0;
            while (fileReader.hasNextLine()) {
                floatArray[positionInArray] = Float.parseFloat(fileReader.nextLine());
                positionInArray++;
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
        return floatArray;
    }
}
